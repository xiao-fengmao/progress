package zut.group1.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zut.group1.dao.StudentDao;
import zut.group1.domain.Student;

import java.util.List;
//import zut.group1.Dao;

@Service
public class testservice {
    @Autowired
    private StudentDao studentDao;
    public List<Student> findAllStudent(){
        return studentDao.findAll();
    }

    public void addStudent(Student student){
        studentDao.add(student);
    }
}

