package zut.group1;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import zut.group1.dao.CatCategoryDao;
import zut.group1.dao.CatDao;
import zut.group1.domain.Cat;
import zut.group1.domain.CatCategory;


import java.util.List;

// JUnit使用Spring方式运行代码，即自动创建spring容器。
@RunWith(SpringJUnit4ClassRunner.class)

@ContextConfiguration(locations="classpath:applicationContext-dao.xml")
public class CatCategoryDaoTest {
    @Autowired
    private CatCategoryDao catCategoryDao;

    private CatCategory catCategory;
    @Test
    public void findAllStudent(){
        List<CatCategory> all = catCategoryDao.findAll();
        all.forEach(System.out::println);
    }


}

