package zut.group1;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import zut.group1.dao.CatCategoryDao;
import zut.group1.dao.CatDao;
import zut.group1.domain.Cat;
import zut.group1.domain.CatCategory;


import java.util.List;

// JUnit使用Spring方式运行代码，即自动创建spring容器。
@RunWith(SpringJUnit4ClassRunner.class)
// 告知创建spring容器时读取哪个配置类或配置文件
// 配置类写法：@ContextConfiguration(classes=配置类.class)
@ContextConfiguration(locations="classpath:applicationContext-dao.xml")
public class CatDaoTest {
    @Autowired
    private CatDao catDao;

    private CatCategory catCategory;
    @Test
    public void findAllStudent(){
        List<Cat> all = catDao.findAll();
        all.forEach(System.out::println);
    }
    @Autowired
    private CatCategoryDao catCategoryDao;
    @Test
    public void addcat(){
        //添加前必须先找到对应的catCategory对象
        CatCategory catCategory = catCategoryDao.findByCategory(2);
        Cat cat = new Cat("mimi", "Male", 1, catCategory, "健康");
        catDao.add(cat);
//        sqlSession.commit();

    }
}