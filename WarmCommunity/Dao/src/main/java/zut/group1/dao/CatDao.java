package zut.group1.dao;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import org.springframework.stereotype.Repository;
import zut.group1.domain.Cat;
import zut.group1.domain.Student;

import java.util.List;

@Repository
public interface CatDao {
    //一对一查询的主表的查询
    @Select("select * from cat")
    @Results(id="catDao",value={
            // public Cat(int id, String name, String sex, int age, CatCategory category, String health) {
            @Result(id = true,property = "id",column = "id"),
            @Result(property = "name",column = "name"),
            @Result(property = "sex",column = "sex"),
            @Result(property = "age",column = "age"),
            //@Result(property = "category.id", column = "category_id"),
            @Result(property = "category",column = "category_id",
                    one = @One(select ="zut.group1.dao.CatCategoryDao.findByCategory",
                            fetchType = FetchType.EAGER
            )),
            @Result(property = "health",column = "health")
    })
    List<Cat> findAll();


    //一对多查询的从表的查询
    @Select("SELECT * from Cat where category_id=#{cid}")
    List<Cat> findbycid(int cid);



    @Insert("insert into cat (name, sex, age, category_id, health) values (#{name}, #{sex}, #{age}, #{category.cid}, #{health})")
    void add(Cat cat);

}


