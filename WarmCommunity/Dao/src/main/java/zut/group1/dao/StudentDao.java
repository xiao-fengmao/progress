package zut.group1.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import zut.group1.domain.Student;

import java.util.List;

@Repository
public interface StudentDao {
    // 查询所有学生
    @Select("select * from student")
    List<Student> findAll();

    // 添加学生,由于主键是自增的所以传参的时候可以null
    @Insert("insert into student values(null,#{name},#{sex},#{address})")
    void add(Student student);
}
