package zut.group1.dao;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import org.springframework.stereotype.Repository;
import zut.group1.domain.CatCategory;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

import zut.group1.domain.Cat;

@Repository
public interface CatCategoryDao {

    @Select("select * from catcategory")
    @Results(id = "catCategoryDao",value={
            @Result(id = true,property = "cid",column = "cid"),
            @Result(property = "name",column = "name"),
            //
            @Result(property = "catList",column = "cid",

            many=@Many(select = "zut.group1.dao.CatDao.findbycid",
                    fetchType= FetchType.LAZY))
    })
    List<CatCategory> findAll();


    //一对一查询的从表的查询
    @Select("select * from catcategory where cid = #{cid}")
    CatCategory findByCategory(Integer cid);

//    @Insert("insert into catcategory values(null, #{name}, null)")
//    void add(CatCategory catCategory);
    @Insert("insert into catcategory (name) values (#{name})")
    void add(CatCategory catCategory);
}