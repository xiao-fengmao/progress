package zut.group1.domain;
/*一只猫咪对应一个品种，这是一对一的关系*/
public class Cat {
    private int id;
    private String name;
    private String sex;
    private int age;
    //private String category;
    private CatCategory category;
    private String health;
    public Cat() {

    }
    public Cat(String name, String sex, int age, CatCategory category, String health) {
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.category = category;
        this.health = health;
    }
    public Cat(int id, String name, String sex, int age, CatCategory category, String health) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.category = category;
        this.health = health;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public CatCategory getCategory() {
        return category;
    }

    public void setCategory(CatCategory category) {
        this.category = category;
    }

    public String getHealth() {
        return health;
    }

    public void setHealth(String health) {
        this.health = health;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", age=" + age +
                ", category=" + category +
                ", health='" + health + '\'' +
                '}';
    }
}

