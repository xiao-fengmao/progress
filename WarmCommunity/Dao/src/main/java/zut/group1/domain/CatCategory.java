package zut.group1.domain;

import java.util.List;

/*一个品种对应多个猫咪，一对多的关系*/
public class CatCategory {
    private int cid;
    private String name;
    private List<Cat> catList;

    public CatCategory() {
    }
    public CatCategory(String name, List<Cat> catList) {
        this.name = name;
        this.catList = catList;
    }

    public CatCategory(int cid, String name, List<Cat> catList) {
        this.cid = cid;
        this.name = name;
        this.catList = catList;
    }


    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Cat> getCatList() {
        return catList;
    }

    public void setCatList(List<Cat> catList) {
        this.catList = catList;
    }

    @Override
    public String toString() {
        return "CatCategory{" +
                "cid=" + cid +
                ", name='" + name + '\'' +
                ", catList=" + catList +
                '}';
    }
}
