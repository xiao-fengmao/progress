create  progress;
use progress;
-- Admin---------------------------------------------------------------------------------
CREATE TABLE Admin (
                       aid INT PRIMARY KEY AUTO_INCREMENT,
                       username VARCHAR(255),
                       password VARCHAR(255),
                       phoneNum VARCHAR(20),
                       email VARCHAR(255),
                       status TINYINT(1),
                       INDEX idx_status (status)
);
-- 向Admin表插入数据
INSERT INTO Admin (username, password, phoneNum, email, status)
VALUES ('admin1', 'password1', '1234567890', 'admin1@example.com', 1),
       ('admin2', 'password2', '0987654321', 'admin2@example.com', 0);

DROP TABLE IF EXISTS `admin_role`;
CREATE TABLE `admin_role` (
                              `aid` varchar(32) NOT NULL,
                              `rid` varchar(32) NOT NULL,
                              PRIMARY KEY (`aid`,`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `admin_role`(`aid`, `rid`) VALUES ('1', '1'), ('2', '2');


CREATE TABLE `permission` (
                              `pid` int(32) NOT NULL AUTO_INCREMENT,
                              `permissionName` varchar(50) DEFAULT NULL,
                              `permissionDesc` varchar(50) DEFAULT NULL,
                              PRIMARY KEY (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `permission` */

insert  into `permission`(`pid`,`permissionName`,`permissionDesc`) values (1,'查询管理员','/admin/all'),(2,'查询角色','/role/all'),(3,'查询权限','/permission/all'),(4,'新增权限','/permission/add');

-- --
-- book--
CREATE TABLE bookcategory (
                              bookcategoryid INT PRIMARY KEY AUTO_INCREMENT,
                              bookcategoryname VARCHAR(255)
);
