package edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Hardware;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Data.DataSetName;

public class Hardware {

    @TableId
    private Integer id;
    private String name;
    private Integer cid;
    @TableField(exist = false)
    private DataSetName dataSetName;
}
