package edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Opencv;



import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.util.List;


public class Algorithm implements Serializable {
    @TableId
    private Integer algorithmID;
    private String algorithmName;
    @TableField(exist = false)
    private List<Method> methods;

    public Integer getAlgorithmID() {
        return algorithmID;
    }

    public String getAlgorithmName() {
        return algorithmName;
    }

    public List<Method> getMethods() {
        return methods;
    }

    public Algorithm(Integer algorithmID, String algorithmName, List<Method> methods) {
        this.algorithmID = algorithmID;
        this.algorithmName = algorithmName;
        this.methods = methods;
    }
    public Algorithm()
    {

    }

    public void setMethods(List<Method> methods) {
        this.methods = methods;
    }

    @Override
    public String toString() {
        return "Algorithm{" +
                "algorithmID=" + algorithmID +
                ", algorithmName='" + algorithmName + '\'' +
                '}';
    }

    public void setAlgorithmID(Integer algorithmID) {
        this.algorithmID = algorithmID;
    }

    public void setAlgorithmName(String algorithmName) {
        this.algorithmName = algorithmName;
    }



}
