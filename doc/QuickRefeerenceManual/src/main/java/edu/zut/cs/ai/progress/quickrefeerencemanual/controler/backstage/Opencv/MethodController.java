package edu.zut.cs.ai.progress.quickrefeerencemanual.controler.backstage.Opencv;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Opencv.Algorithm;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Opencv.Method;
import edu.zut.cs.ai.progress.quickrefeerencemanual.service.Opencv.AlgorithmService;
import edu.zut.cs.ai.progress.quickrefeerencemanual.service.Opencv.MethodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/backstage/algorithm")
public class MethodController {

    @Autowired
    public void setMethodService(MethodService methodService) {
        this.methodService = methodService;
    }

    private MethodService methodService;

//    @RequestMapping("/add")
//    public String add(@RequestParam("algorithmID") Integer algorithmID, Method method) {
//        // 这里可以使用 algorithmID 进行相应的操作
//        methodService.save(method);
//        return "redirect:/backstage/opencv/desc?algorithmID=" + algorithmID;
//    }


    @RequestMapping("/add")
    public String add(Integer algorithmID,Method method) {
        methodService.save(algorithmID,method);
        return "redirect:/backstage/opencv/desc?algorithmID="+algorithmID;
    }

    @RequestMapping("/all")
    public ModelAndView all(@RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "10") int size) {
        Page<Method> methodPage = methodService.findPage(page, size);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("methodPage", methodPage);
        modelAndView.setViewName("/backstage/method_all");
        return modelAndView;
    }
    // 查询管理员，跳转到修改页面
    @RequestMapping("/edit")
    public ModelAndView edit(Integer methodID) {
        Method method = methodService.selectById(methodID);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("method", method);
        modelAndView.setViewName("/backstage/method_edit");
        return modelAndView;
    }
    @RequestMapping("/update")
    public String update(Method method) {
        methodService.update(method);
        return "redirect:/backstage/opencv/all";
    }
    @RequestMapping("/delete")
    public String delete(Method method) {
//        boolean flag = algorithmService.selectMethod(algorithm.getAlgorithmID());
//        if (flag)
//        {
//            algorithmService.delete(algorithm);
//            return "redirect:/backstage/opencv/all";
//        }
//        else
//        {
        methodService.delete(method.getMethodID());
        return "redirect:/backstage/opencv/all";

    }

//    @Autowired
//    AlgorithmService algorithmService;
//    @RequestMapping("/edit")
//    public ModelAndView edit(@PathVariable Integer algorithmID,Integer methodID) {
//        Method method = methodService.selectById(methodID);
//        ModelAndView modelAndView = new ModelAndView();
//        modelAndView.addObject("method", method);
//        modelAndView.setViewName("/backstage/method_edit");
//        return modelAndView;
//    }


    // 修改管理员
//    @RequestMapping("/update")
//    public String update(Method method,@RequestParam("algorithmID") Integer algorithmID) {
//        AlgorithmController algorithmController = new AlgorithmController();
//        methodService.update(method);
//        return "redirect:/backstage/opencv/desc?algorithmID="+algorithmID;
//    }
//    @RequestMapping("/backstage/opencv/desc")
//    public void desc(Integer algorithmID) {
//        ID = algorithmID;
//    }
//


//    @PostMapping
//    public Result save(@RequestBody Method method) {
//        boolean flag = methodService.save(method);
//        return new Result(flag ? Code.SAVE_OK : Code.SAVE_ERR, flag);
//    }
//
//    @PutMapping
//    public Result update(@RequestBody Method method) {
//        boolean flag = methodService.save(method);
//        return new Result(flag ? Code.SAVE_OK : Code.SAVE_ERR, flag);
//    }
//
//    @DeleteMapping("/{id}")
//    public Result delete(@PathVariable Integer id) {
//        boolean flag = methodService.delete(id);
//        return new Result(flag ? Code.SAVE_OK : Code.SAVE_ERR, flag);
//    }
//
//
//    @GetMapping("/{id}")
//    public Result selectById(@PathVariable int id) {
//        Method method = methodService.selectById(id);
//        Integer code = method != null ? Code.GET_OK : Code.GET_ERR;
//        String msg = method != null ? "" : "数据查询失败，请重试！";
//        return new Result(code, method, msg);
//
//    }
//
//    @GetMapping
//    public Result findAll() {
//
//        List<Method> methods = methodService.findAll();
//        Integer code = methods != null ? Code.GET_OK : Code.GET_ERR;
//        String msg = methods != null ? "" : "数据查询失败，请重试！";
//        return new Result(code, methods, msg);
//    }


}
