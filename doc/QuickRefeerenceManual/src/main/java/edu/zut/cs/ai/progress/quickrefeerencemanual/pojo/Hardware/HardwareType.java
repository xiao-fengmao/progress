package edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Hardware;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

public class HardwareType {
    @TableId(type = IdType.AUTO)
    private int nid;
    private String name;

    public HardwareType(String name) {
        this.name = name;
    }
}
