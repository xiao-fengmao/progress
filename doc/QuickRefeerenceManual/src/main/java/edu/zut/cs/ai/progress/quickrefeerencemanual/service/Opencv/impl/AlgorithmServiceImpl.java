package edu.zut.cs.ai.progress.quickrefeerencemanual.service.Opencv.impl;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Opencv.AlgorithmDao;
import edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Opencv.MethodDao;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Opencv.Algorithm;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Opencv.Method;
import edu.zut.cs.ai.progress.quickrefeerencemanual.service.Opencv.AlgorithmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AlgorithmServiceImpl implements AlgorithmService {

    @Autowired
    public void setAlgorithmDao(AlgorithmDao algorithmDao) {
        this.algorithmDao = algorithmDao;
    }

    @Autowired
    public void setMethod(MethodDao methodDao) {
        this.methodDao = methodDao;
    }

    private AlgorithmDao algorithmDao;

    private MethodDao methodDao;

    public AlgorithmServiceImpl(AlgorithmDao algorithmDao, MethodDao methodDao) {
        this.algorithmDao = algorithmDao;
        this.methodDao = methodDao;
    }

    @Override
    public void save(Algorithm algorithm) {
        algorithmDao.save(algorithm);
//        List<Method> methods = algorithm.getMethods();
//        for (Method method : methods) {
//           algorithmDao.saveAssociate(algorithm.getAlgorithmID(), method.getMethodID());
//
//        }
    }

    public Page<Algorithm> findPage(int page, int size) {
        //页码数，和每页条数，查询条件为null,
        Page selectPage = algorithmDao.selectPage(new Page(page, size), null);
        return selectPage;
    }


    @Override
    public void delete(Algorithm algorithm) {
        algorithmDao.delete(algorithm.getAlgorithmID());


    }

    @Override
    public boolean selectMethod(Integer algorithmID) {
        return false;
    }

    @Override
    public List<Method> findDesc(Integer algorithmID) {
        List<Method> methods = new ArrayList<>();
        List<Integer> IDs = algorithmDao.selectAllMethodIdById(algorithmID);
        for(Integer ID:IDs)
        {
            Method method = methodDao.selectById(ID);
            methods.add(method);
        }

        return methods;
    }

    @Override
    public void update(Algorithm algorithm) {
        algorithmDao.update(algorithm.getAlgorithmID(), algorithm.getAlgorithmName());

    }

    @Override
    public List<Algorithm> findAll() {
        return algorithmDao.findAll();
    }

    @Override
    public Algorithm selectById(int algorithmID) {
        return algorithmDao.selectById(algorithmID);

    }

    @Override
    public void selectAllMethod(String algorithmName) {
        List<Algorithm> algorithms = algorithmDao.selectByName(algorithmName);
        for (Algorithm algorithm : algorithms) {
            List<Integer> methodIds = algorithmDao.selectAllMethodIdById(algorithm.getAlgorithmID());
            if (methodIds == null) {
                System.out.println("没有查询到" + algorithmDao.selectById(algorithm.getAlgorithmID()).getAlgorithmName() + "算法的函数");
            } else {
                System.out.println(algorithmDao.selectById(algorithm.getAlgorithmID()).getAlgorithmName() + "算法对应的所有方法有：");
                for (Integer methodId : methodIds) {
                    System.out.println(methodDao.selectById(methodId));
                }
            }
            System.out.println("-----------------------------------------------------------------");


        }
    }

    @Override
    public List<Algorithm> selectByName(String algorithmName) {
        return algorithmDao.selectByName(algorithmName);
    }


    public void delete(int id) {
        List<Integer> algorithmIds = algorithmDao.selectAssociateById(id);
        if (!algorithmIds.isEmpty()) {

            for (int algorithmId : algorithmIds) {
                algorithmDao.deleteAssociate(algorithmId);
            }

            algorithmDao.delete(id);
        } else {
            algorithmDao.delete(id);
        }


    }


}

