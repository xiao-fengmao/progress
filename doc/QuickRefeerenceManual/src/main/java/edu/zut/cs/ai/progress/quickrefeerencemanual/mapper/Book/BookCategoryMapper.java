package edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Book;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Admin.Admin;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Book.BookCategory;

import java.util.List;

public interface BookCategoryMapper extends BaseMapper<BookCategory> {
//    一对多的多表查询


}
