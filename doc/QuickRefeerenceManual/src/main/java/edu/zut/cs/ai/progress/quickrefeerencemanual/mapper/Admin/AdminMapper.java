package edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Admin;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Admin.Admin;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.transaction.annotation.Transactional;
@Mapper
public interface AdminMapper extends BaseMapper<Admin> {
    Admin findDesc(Integer aid);
}

//edu/zut/cs/ai/progress/quickrefeerencemanual/mapper
//edu/zut/cs/ai/progress/quickrefeerencemanual/mapper/Admin/AdminMapper.java
//edu/zut/cs/ai/progress/quickrefeerencemanual/mapper/AdminMapper.xml
//edu/zut/cs/ai/progress/quickrefeerencemanual/mapper/Admin/AdminMapper.xml
//'edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Admin.AdminMapper
//'edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.AdminMapper'