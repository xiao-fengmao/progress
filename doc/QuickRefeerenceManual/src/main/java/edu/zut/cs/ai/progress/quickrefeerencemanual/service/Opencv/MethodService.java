package edu.zut.cs.ai.progress.quickrefeerencemanual.service.Opencv;



import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Opencv.Method;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface MethodService {
    @Transactional
    void save(Integer algorithmID,Method method);

    void delete(Integer methodID);

    void update(Method method);

    List<Method> findAll();

    List<Method> findByContent(String content);

    Method selectById(int id);

    Page<Method> findPage(int page, int size);
}
