package edu.zut.cs.ai.progress.quickrefeerencemanual.service.Opencv;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Admin.Admin;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Opencv.Algorithm;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Opencv.Method;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface AlgorithmService {
    @Transactional
    void save(Algorithm algorithm);


    void delete(Algorithm algorithm);

    boolean selectMethod(Integer algorithmID);

    void update(Algorithm algorithm);

    List<Algorithm> findAll();

    Algorithm selectById(int algorithmID);

    void selectAllMethod(String algorithmName);

    List<Algorithm> selectByName(String algorithmName);
    void delete(int id);


    Page<Algorithm> findPage(int page, int size);

    List<Method> findDesc(Integer algorithmID);
}
