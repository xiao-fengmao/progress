package edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Opencv;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Opencv.Method;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MethodDao extends BaseMapper<Method> {

    @Insert("insert into method(methodID,methodName,content)values(#{methodID},#{methodName},#{content})")
    void saveMethod(Method method);

    @Insert("insert into algorithmassociatemethod(algorithmID,methodID)values(#{algorithmID},#{methodID})")
    void saveAssociate(@Param("algorithmID")Integer algorithmID, @Param("methodID")Integer methodID);

    @Delete("delete from method where methodID = #{methodID} ")
    void delete(@Param("methodID") Integer methodID);

    @Delete("delete from algorithmassociatemethod where algorithmID = #{algorithmID} and methodID = #{methodID} ")
    void deleteAssociate(@Param("algorithmID") Integer algorithmID,@Param("methodID") Integer methodID);

    @Update("UPDATE method SET methodName = #{methodName}, content = #{content} WHERE methodID = #{methodID}")
    void update(Method method);


    @Select("select * from method")
    List<Method> findAll();

    @Select("select * from method where content like #{content} ")
    List<Method> findByContent(String content);

    @Select("select * from method where methodID = #{id}")
    Method selectById(int id);


    @Select("select algorithmID from algorithmassociatemethod where methodID = #{methodID}")
    List<Integer> selectAllAlgorithmIDById(@Param("methodID") Integer methodID);
}