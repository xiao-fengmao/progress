package edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Opencv;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import java.util.List;

public class Method {
    @TableId
    private Integer methodID;
    private String methodName;
    private String content;
    @TableField(exist = false)
    private List<Algorithm> algorithms;



    public List<Algorithm> getAlgorithms() {
        return algorithms;
    }

    public void setAlgorithms(List<Algorithm> algorithms) {
        this.algorithms = algorithms;
    }

    public Method()
{

}

    public Method(Integer methodID, String methodName, String content, List<Algorithm> algorithms) {
        this.methodID = methodID;
        this.methodName = methodName;
        this.content = content;
        this.algorithms = algorithms;
    }

    @Override
    public String toString() {
        return "Method{" +
                "methodID=" + methodID +
                ", methodName='" + methodName + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

    public Integer getMethodID() {
        return methodID;
    }

    public void setMethodID(Integer methodID) {
        this.methodID = methodID;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
