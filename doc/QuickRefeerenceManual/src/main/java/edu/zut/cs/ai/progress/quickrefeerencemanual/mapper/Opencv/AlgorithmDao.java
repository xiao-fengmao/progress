package edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Opencv;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Opencv.Algorithm;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Opencv.Method;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AlgorithmDao extends BaseMapper<Algorithm> {

    @Insert("insert into algorithm(algorithmID,algorithmName)values(#{algorithmID},#{algorithmName})")
    void save(Algorithm algorithm);
    @Insert("insert into algorithmassociatemethod(algorithmID,methodID)values(#{arg0},#{arg1})")
    void saveAssociate(int algorithmID,int methodID);
    @Delete("delete from algorithm where algorithmID = #{algorithmID} ")
    void delete(@Param("algorithmID") Integer algorithmID);
    @Delete("delete from algorithmassociatemethod where algorithmID = #{algorithmID} ")
    void deleteAssociate(@Param("algorithmID") Integer id);
    @Update("update algorithm set algorithmName = #{algorithmName} where algorithmID = #{algorithmID}")
    void update(@Param("algorithmID")int algorithmID,@Param("algorithmName")String algorithmName);

    @Select("select * from algorithm")
    List<Algorithm> findAll();

    @Select("select * from algorithm where algorithmID = #{algorithmID}")
    Algorithm selectById(@Param("algorithmID")int algorithmID);
    @Select("select algorithmID from algorithmassociatemethod where algorithmID = #{id}")
    List<Integer> selectAssociateById(int id);


    @Select("select * from algorithm where algorithmName like #{algorithmName}")
    List<Algorithm> selectByName(String algorithmName);


    @Select("select methodID from algorithmassociatemethod where algorithmID = #{id}")
    List<Integer> selectAllMethodIdById(Integer id);
    @Select("select * from algorithmassociatemethod where algorithmID = #{algorithmID}")
    List<Method> selectAllMethodsById(@Param("algorithmID")int algorithmID);

}