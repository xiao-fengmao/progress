package edu.zut.cs.ai.progress.quickrefeerencemanual.service.Data;

import edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Data.DataSetNameMapper;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Data.DataSetName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataSetNameService {
    @Autowired
    private DataSetNameMapper dataSetNameMapper;
    public void add(DataSetName dataSetName){
        dataSetNameMapper.insert(dataSetName);

    }

}
