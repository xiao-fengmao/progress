package edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Data;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("dataSet")
public class DataSet {
    @TableId
    private Integer id;
    private String name;
    private Integer cid;
    @TableField(exist = false)
    private DataSetName dataSetName;


}