package edu.zut.cs.ai.progress.quickrefeerencemanual.Openvc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Opencv.Method;

import java.util.List;

public interface MethodMapper extends BaseMapper<Method> {
    // 查询用户拥有的所有角色的id
    List<Integer> findMethodIdByAlgorithm(Integer aid);
}

