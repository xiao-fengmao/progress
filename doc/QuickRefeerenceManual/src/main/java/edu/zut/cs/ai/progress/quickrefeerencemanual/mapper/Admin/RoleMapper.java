package edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Admin;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Admin.Role;

import java.util.List;

public interface RoleMapper extends BaseMapper<Role> {
    // 查询用户拥有的所有角色的id
    List<Integer> findRoleIdByAdmin(Integer aid);
}

