package edu.zut.cs.ai.progress.quickrefeerencemanual.service.Opencv.impl;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Opencv.MethodDao;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Opencv.Algorithm;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Opencv.Method;
import edu.zut.cs.ai.progress.quickrefeerencemanual.service.Opencv.MethodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MethodServiceImpl implements MethodService {

    @Autowired
    public void setMethodDao(MethodDao methodDao) {
        this.methodDao = methodDao;
    }

    private MethodDao methodDao;

    public MethodServiceImpl(MethodDao methodDao) {
        this.methodDao = methodDao;
    }

    @Override
    public void save(Integer algorithmID,Method method) {
        Method method1 = methodDao.selectById(method.getMethodID());
        if(method1==null) {
            methodDao.saveMethod(method);
//            List<Integer> algorithmIDs = methodDao.selectAllAlgorithmIDById(method.getMethodID());
//            for (Integer algorithmID : algorithmIDs) {
                methodDao.saveAssociate(algorithmID, method.getMethodID());
//            }
        }
        else {
//            List<Integer> algorithmIDs = methodDao.selectAllAlgorithmIDById(method.getMethodID());
//            for (Integer algorithmID : algorithmIDs) {
                methodDao.saveAssociate(algorithmID, method.getMethodID());

            }
//        }

    }

    @Override
    public void delete(Integer methodID) {
        List<Integer> algorithmIDs = methodDao.selectAllAlgorithmIDById(methodID);
        if (!algorithmIDs.isEmpty()) {
            for (Integer algorithmID : algorithmIDs) {
                methodDao.deleteAssociate(algorithmID,methodID);
            }

            methodDao.delete(methodID);
        } else {
            methodDao.delete(methodID);
        }



    }

    @Override
    public void update(Method method) {
        methodDao.update(method);

    }

    @Override
    public List<Method> findAll() {
        return methodDao.findAll();
    }

    @Override
    public List<Method> findByContent(String content) {
        return methodDao.findByContent(content);
    }

    @Override
    public Method selectById(int id) {
        return methodDao.selectById(id);
    }

    @Override
    public Page<Method> findPage(int page, int size) {
        //页码数，和每页条数，查询条件为null,
        Page selectPage = methodDao.selectPage(new Page(page, size), null);
        return selectPage;
    }

}
