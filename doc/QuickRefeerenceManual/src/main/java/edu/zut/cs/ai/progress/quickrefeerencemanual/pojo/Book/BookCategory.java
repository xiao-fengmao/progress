package edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Book;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Admin.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("bookcategory")
public class BookCategory {
    @TableId
    private Integer bookcategoryid;
    private String bookcategoryname;
    @TableField(exist = false) // 不是数据库的字段
    private List<Book> book; // 角色集合

    public BookCategory(String bookcategoryname) {
        this.bookcategoryname = bookcategoryname;
    }
}
