package edu.zut.cs.ai.progress.quickrefeerencemanual.Openvc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Admin.Admin;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Opencv.Algorithm;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AlgorithmMapper extends BaseMapper<Algorithm> {
    Algorithm findDesc(Integer aid);
}

//edu/zut/cs/ai/progress/quickrefeerencemanual/mapper
//edu/zut/cs/ai/progress/quickrefeerencemanual/mapper/Admin/AdminMapper.java
//edu/zut/cs/ai/progress/quickrefeerencemanual/mapper/AdminMapper.xml
//edu/zut/cs/ai/progress/quickrefeerencemanual/mapper/Admin/AdminMapper.xml
//'edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Admin.AdminMapper
//'edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.AdminMapper'