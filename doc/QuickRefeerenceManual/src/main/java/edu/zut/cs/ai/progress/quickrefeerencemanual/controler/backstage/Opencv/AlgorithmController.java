package edu.zut.cs.ai.progress.quickrefeerencemanual.controler.backstage.Opencv;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Admin.Admin;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Opencv.Algorithm;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Opencv.Method;
import edu.zut.cs.ai.progress.quickrefeerencemanual.service.Opencv.AlgorithmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/backstage/opencv")
public class AlgorithmController {
    private AlgorithmService algorithmService;

    @Autowired
    public void setAlgorithmService(

            AlgorithmService algorithmService) {
        this.algorithmService = algorithmService;
    }

    @RequestMapping("/all")
    public ModelAndView all(@RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "10") int size) {
        Page<Algorithm> algorithmPage = algorithmService.findPage(page, size);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("algorithmPage", algorithmPage);
        modelAndView.setViewName("/backstage/algorithm_all");
        return modelAndView;
    }

    @RequestMapping("/add")
    public String add(Algorithm algorithm) {
        algorithmService.save(algorithm);
        return "redirect:/backstage/opencv/all";
    }
    @RequestMapping("/desc")
    public ModelAndView desc(Integer algorithmID){
        List<Method> methods = algorithmService.findDesc(algorithmID);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("methods", methods);
        modelAndView.setViewName("/backstage/algorithm_desc");
        return modelAndView;
    }

    // 查询管理员，跳转到修改页面
    @RequestMapping("/edit")
    public ModelAndView edit(Integer algorithmID) {
        Algorithm algorithm = algorithmService.selectById(algorithmID);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("algorithm", algorithm);
        modelAndView.setViewName("/backstage/algorithm_edit");
        return modelAndView;
    }

    // 修改管理员
    @RequestMapping("/update")
    public String update(Algorithm algorithm) {
        algorithmService.update(algorithm);
        return "redirect:/backstage/opencv/all";
    }


    @RequestMapping("/delete")
    public String delete(Algorithm algorithm) {
//        boolean flag = algorithmService.selectMethod(algorithm.getAlgorithmID());
//        if (flag)
//        {
//            algorithmService.delete(algorithm);
//            return "redirect:/backstage/opencv/all";
//        }
//        else
//        {
        algorithmService.delete(algorithm.getAlgorithmID());
        return "redirect:/backstage/opencv/all";

    }

//
//
//    @GetMapping
//    public Result findAll() {
//        List<Algorithm> algorithms = algorithmService.findAll();
//        Integer code = algorithms != null ? Code.GET_OK : Code.GET_ERR;
//        String msg = algorithms != null ? "" : "数据查询失败，请重试！";
//        return new Result(code, algorithms, msg);
//    }
//
//    @GetMapping("/{id}")
//    public Result selectById(@PathVariable int id) {
//        Algorithm algorithm = algorithmService.selectById(id);
//        Integer code = algorithm != null ? Code.GET_OK : Code.GET_ERR;
//        String msg = algorithm != null ? "" : "数据查询失败，请重试！";
//        return new Result(code, algorithm, msg);
//
//    }
//
//    @GetMapping("/{algorithmName}")
//    public Result selectAllMethod(@PathVariable String algorithmName) {
//        boolean flag = algorithmService.selectAllMethod(algorithmName);
//        return new Result(flag ? Code.GET_OK : Code.GET_ERR, flag);
//
//    }
//
//    @GetMapping("/{algorithmName}")
//    public Result selectByName(@PathVariable String algorithmName) {
//        List<Algorithm> algorithms = algorithmService.selectByName(algorithmName);
//        Integer code = algorithms != null ? Code.GET_OK : Code.GET_ERR;
//        String msg = algorithms != null ? "" : "数据查询失败，请重试！";
//        return new Result(code, algorithms, msg);
//
//    }


}
