package edu.zut.cs.ai.progress.quickrefeerencemanual.service;


import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Opencv.Algorithm;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Opencv.Method;
import edu.zut.cs.ai.progress.quickrefeerencemanual.service.Opencv.AlgorithmService;
import edu.zut.cs.ai.progress.quickrefeerencemanual.service.Opencv.MethodService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.ArrayList;
import java.util.List;

//设置类运行器
@SpringBootTest
public class AlgorithmServiceTest {


    //todo 支持自动装配注入bean
    @Autowired
    private AlgorithmService algorithmService;
    @Autowired
    private MethodService methodService;

    @Test
    public void testFindByContent() {
        String content = "阈值";
        content = "%" + content + "%";
        System.out.println(methodService.findByContent(content));

    }

    @Test
    public void testFindAll() {
        System.out.println(methodService.findAll());
    }

    @Test
    public void testSelectById() {
        System.out.println(algorithmService.selectById(1));
    }

    @Test
    public void testDelete() {
        int id = 5;
        algorithmService.delete(id);
    }

    @Test
    public void testUpdate() {
        int id = 3;
        Algorithm algorithm = algorithmService.selectById(id);
        algorithmService.update(algorithm);
    }

    @Test
    public void testDelete2() {
        int id = 11;
        methodService.delete(id);
    }

    @Test
    public void testSelectAllMethod() {
        String algorithmName = "图像增强";
        algorithmName = "%" + algorithmName + "%";
        algorithmService.selectAllMethod(algorithmName);
    }
    @Test
    public void testfinddesc() {
        algorithmService.findDesc(1);

    }
//    @Test
//    public void testSaveMethod() {
//
//        methodService.save(1);
//
//    }
//    @Test
//    public void testSave()
//// todo 用事务实现同时实现同时停止
//    {
//        int algorithmID;
//        int id = 4;
//        String name = "直方图阈值";
//        String content = "直方图的内容";
//
//        Method method = new Method();
//        method.setMethodName(name);
//        method.setMethodID(id);
//        method.setContent(content);
//        List<Algorithm> algorithms = new ArrayList<>();
//        algorithmID = 2;
//        if (algorithmID != 0) {
//            Algorithm algorithm = algorithmService.selectById(2);
//            if (algorithm != null) {
//                algorithms.add(algorithm);
//            }
//        }
//
//
//        method.setAlgorithms(algorithms);
//        methodService.save(method);
//
//    }


}
