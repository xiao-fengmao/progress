package edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Admin;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
/**
 * 管理员与角色是多对多的关系
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("Admin")
public class Admin {
    @TableId
    private Integer aid;
    private String username;
    private String password;
    private String phoneNum;
    private String email;
    private boolean status; // 状态 true可用 false禁用
    @TableField(exist = false) // 不是数据库的字段
    private List<Role> roles; // 角色集合

    public Admin( String username, String password, String phoneNum, String email, boolean status) {

        this.username = username;
        this.password = password;
        this.phoneNum = phoneNum;
        this.email = email;
        this.status = status;
        this.roles = roles;
    }
}
