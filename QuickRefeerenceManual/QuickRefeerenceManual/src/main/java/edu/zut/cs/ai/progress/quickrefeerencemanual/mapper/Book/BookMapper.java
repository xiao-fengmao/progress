package edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Book;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Book.Book;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface BookMapper extends BaseMapper<Book>{
    Page<Book> findbookPage(Page<Book> page);

}
