package edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Data;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;


@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("dataname")
public class DataSetName {
    @TableId(type = IdType.AUTO)
    private int nid;
    private String name;

    public DataSetName(String name) {
        this.name = name;
    }
}
