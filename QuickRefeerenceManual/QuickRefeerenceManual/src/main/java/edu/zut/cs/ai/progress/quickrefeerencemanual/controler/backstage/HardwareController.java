package edu.zut.cs.ai.progress.quickrefeerencemanual.controler.backstage;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Book.Book;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Book.BookCategory;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Hardware.Hardware;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Hardware.HardwareType;
import edu.zut.cs.ai.progress.quickrefeerencemanual.service.Hardware.HardwareService;
import edu.zut.cs.ai.progress.quickrefeerencemanual.service.Hardware.HardwareTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/backstage/hardware")
public class HardwareController {
    @Autowired
    HardwareService hardwareService;
    @Autowired
    HardwareTypeService hardwareTypeService;
    @RequestMapping("/all")
    public ModelAndView all(@RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "5") int size) {
        Page<Hardware> hardPage = hardwareService.findPage(page, size);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("hardPage", hardPage);
        modelAndView.setViewName("/backstage/hardware_all");
        return modelAndView;
    }

    @RequestMapping("/addPage")
    public ModelAndView addPage() {
        // 查询所有产品类别
        List<HardwareType> categoryList = hardwareTypeService.findAll();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("categoryList", categoryList);
        modelAndView.setViewName("/backstage/hardware_add");
        return modelAndView;
    }

    @RequestMapping("/add")
    public String add(Hardware hardware) {
        hardwareService.add(hardware);
        return "redirect:/backstage/hardware/all";
    }
    @RequestMapping("/delete")
    public String delete(Integer id){
        hardwareService.delete(id);
        return "redirect:/backstage/hardware/all";
    }
}
