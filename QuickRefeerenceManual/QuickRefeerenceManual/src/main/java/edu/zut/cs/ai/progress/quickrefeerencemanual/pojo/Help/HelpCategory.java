package edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Help;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("HelpCategory")
public class HelpCategory {
    @TableId
    private Integer helpcategoryid;
    private String helpcategoryname;
    @TableField(exist = false) // 不是数据库的字段
    private List<Help> helps; // 角色集合
//    }
}