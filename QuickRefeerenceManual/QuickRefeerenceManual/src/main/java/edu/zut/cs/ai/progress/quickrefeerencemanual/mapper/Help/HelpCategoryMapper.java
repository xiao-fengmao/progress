package edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Help;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Help.HelpCategory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


@Mapper
public interface HelpCategoryMapper extends BaseMapper<HelpCategory> {
    HelpCategory findhelp(@Param("cid") Integer cid);
}
