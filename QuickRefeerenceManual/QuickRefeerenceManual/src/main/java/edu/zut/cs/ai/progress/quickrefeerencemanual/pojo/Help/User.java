package edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Help;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private Integer id; //ID
    private String username; //用户名
    private String password; //密码
    private  Short gender;  //性别 ，1男 ，2女
}
