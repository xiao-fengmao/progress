package edu.zut.cs.ai.progress.quickrefeerencemanual.service.Help;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Help.HelpMapper;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Help.Help;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HelpService {
    @Autowired
    HelpMapper helpMapper;


    public Page<Help> findPage(int page, int size){
        Page selectPage = helpMapper.findhelpPage(new Page(page, size));
        return selectPage;
    }
    public void add(Help help){
        helpMapper.insert(help);
    }
    public void delete(Integer cid){
        helpMapper.deleteById(cid);
    }
}