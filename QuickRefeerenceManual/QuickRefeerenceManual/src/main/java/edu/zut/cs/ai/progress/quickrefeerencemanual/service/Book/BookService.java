package edu.zut.cs.ai.progress.quickrefeerencemanual.service.Book;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Book.BookMapper;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Book.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class BookService {
    @Autowired
    private BookMapper bookMapper;

    public Page<Book> findPage(int page, int size){
        Page selectPage = bookMapper.findbookPage(new Page(page, size));
        return selectPage;
    }
    public void add(Book book){
       bookMapper.insert(book);
    }
    public void delete(Integer cid){
        bookMapper.deleteById(cid);
    }


}
