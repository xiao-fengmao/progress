package edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Book;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("book")
public class Book {
    @TableId
    private Integer bookid;
    private String bookname;
    private String bookimage;
    private String bookDesc;//详情
    private String Tel;//电话号码
    private BigDecimal price;
    private Integer bookcategoryid;

//    public Book(String bookname, String bookimage, String bookDesc, String tel, BigDecimal price, Integer bookcategoryid) {
//        this.bookname = bookname;
//        this.bookimage = bookimage;
//        this.bookDesc = bookDesc;
//        Tel = tel;
//        this.price = price;
//        this.bookcategoryid = bookcategoryid;
//    }
//    public Book(String bookname, String bookimage, String bookDesc, String tel, BigDecimal price, Integer bookcategoryid, BookCategory bookCategory) {
//        this.bookname = bookname;
//        this.bookimage = bookimage;
//        this.bookDesc = bookDesc;
//        this.Tel = tel;
//        this.price = price;
//        this.bookcategoryid = bookcategoryid;
//        this.bookCategory = bookCategory;
//    }


    @TableField(exist = false)
    private BookCategory bookCategory;
}
