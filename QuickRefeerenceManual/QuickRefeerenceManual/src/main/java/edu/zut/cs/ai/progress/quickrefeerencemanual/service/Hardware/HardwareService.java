package edu.zut.cs.ai.progress.quickrefeerencemanual.service.Hardware;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Hardware.HardwareMapper;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Book.Book;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Hardware.Hardware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HardwareService {
    @Autowired
    HardwareMapper hardwareMapper;
    public Page<Hardware> findPage(int page, int size){
        Page selectPage = hardwareMapper.findhardwarePage(new Page(page, size));
        return selectPage;
    }
    public void add(Hardware hardware){
        hardwareMapper.insert(hardware);
    }
    public void delete(Integer cid){
        hardwareMapper.deleteById(cid);
    }

}
