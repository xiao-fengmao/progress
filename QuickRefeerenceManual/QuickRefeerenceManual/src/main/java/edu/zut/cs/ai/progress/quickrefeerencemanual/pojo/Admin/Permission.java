package edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Admin;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

//权限与角色是多对多的关系
@Data
public class Permission {
    @TableId
    private Integer pid;
    private String permissionName; // 权限名
    private String permissionDesc;//权限详情
}