package edu.zut.cs.ai.progress.quickrefeerencemanual.controler.backstage;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Help.Help;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Help.HelpCategory;
import edu.zut.cs.ai.progress.quickrefeerencemanual.service.Help.HelpCategoryService;
import edu.zut.cs.ai.progress.quickrefeerencemanual.service.Help.HelpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;



@Controller
@RequestMapping("/backstage/help")
public class HelpController {
    @Autowired
    HelpService helpService;

    @Autowired
    HelpCategoryService helpCategoryService;

    @RequestMapping("/all")
    public ModelAndView all(@RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "5") int size) {
        Page<Help> helpPage = helpService.findPage(page, size);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("helpPage", helpPage);
        modelAndView.setViewName("/backstage/help_all");
        return modelAndView;
    }

    @RequestMapping("/addPage")
    public ModelAndView addPage() {
        // 查询所有产品类别
        List<HelpCategory> categoryList = helpCategoryService.findAll();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("categoryList", categoryList);
        modelAndView.setViewName("/backstage/help_add");
        return modelAndView;
    }

    @RequestMapping("/add")
    public String add(Help help) {
        helpService.add(help);
        return "redirect:/backstage/help/all";
    }
    @RequestMapping("/delete")
    public String delete(Integer helpid){
        helpService.delete(helpid);
        return "redirect:/backstage/help/all";
    }
}
