package edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Help;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Hardware.HardwareType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("Help")
public class Help {
    @TableId
    private Integer helpid;
    private String time;
    private String helpdesc;//详情
    private String Tel;//电话号码
    private BigDecimal price;
    private  Boolean statue;
    private Integer helpcategoryid;
    @TableField(exist = false)
    private HelpCategory helpCategory;

}
