package edu.zut.cs.ai.progress.quickrefeerencemanual.controler.backstage;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Admin.Admin;
import edu.zut.cs.ai.progress.quickrefeerencemanual.service.Admin.AdminService;
import edu.zut.cs.ai.progress.quickrefeerencemanual.util.RoleWithStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/backstage/admin")
public class AdminController {
    @Autowired
    private AdminService adminService;
    @RequestMapping("/all")
    public ModelAndView all(@RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "10") int size) {
        Page<Admin> adminPage = adminService.findPage(page, size);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("adminPage", adminPage);
        modelAndView.setViewName("/backstage/admin_all");
        return modelAndView;
    }
    @RequestMapping("/add")
    public String add(Admin admin) {
        adminService.add(admin);
        return "redirect:/backstage/admin/all";
    }
    // 查询管理员，跳转到修改页面
    @RequestMapping("/edit")
    public ModelAndView edit(Integer aid) {
        Admin admin = adminService.findById(aid);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("admin", admin);
        modelAndView.setViewName("/backstage/admin_edit");
        return modelAndView;
    }
    // 修改管理员
    @RequestMapping("/update")
    public String update(Admin admin) {
        adminService.update(admin);
        return "redirect:/backstage/admin/all";
    }
    @RequestMapping("/desc")
    public ModelAndView desc(Integer aid){
        Admin admin = adminService.findDesc(aid);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("admin", admin);
        modelAndView.setViewName("/backstage/admin_desc");
        return modelAndView;
    }
    @RequestMapping("/findRole")
    public ModelAndView findRole(Integer aid) {
        List<RoleWithStatus> roles = adminService.findRole(aid);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("roles", roles);
        modelAndView.addObject("aid", aid);
        modelAndView.setViewName("/backstage/admin_role");
        return modelAndView;
    }
    @RequestMapping("/updateRole")
    public String manageRole(Integer aid,Integer[] ids){
        adminService.updateRoles(aid,ids);
        return "redirect:/backstage/admin/all";
    }
    @RequestMapping("/updateStatus")
    public String updateStatus(Integer aid){
        adminService.updateStatus(aid);
        return "redirect:/backstage/admin/all";
    }






}

