package edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Help;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Help.Help;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface HelpMapper extends BaseMapper<Help> {
    Page<Help> findhelpPage(Page<Help> page);
}
