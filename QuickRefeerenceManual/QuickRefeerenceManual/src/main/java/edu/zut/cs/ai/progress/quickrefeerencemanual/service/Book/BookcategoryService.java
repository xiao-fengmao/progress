package edu.zut.cs.ai.progress.quickrefeerencemanual.service.Book;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Book.BookCategoryMapper;
import edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Book.BookMapper;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Admin.Admin;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Admin.Role;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Book.Book;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Book.BookCategory;
import edu.zut.cs.ai.progress.quickrefeerencemanual.util.RoleWithStatus;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookcategoryService {
    @Autowired
    BookCategoryMapper bookCategoryMapper;
    BookMapper bookMapper;

    public Page<BookCategory> findPage(int page, int size){
        //页码数，和每页条数，查询条件为null,
        Page selectPage = bookCategoryMapper.selectPage(new Page(page, size), null);
        return selectPage;
    }
    public void add(BookCategory bookCategory){
        bookCategoryMapper.insert(bookCategory);
    }
    // 查询管理员
    public BookCategory findById(Integer bookcategoryid){return bookCategoryMapper.selectById(bookcategoryid);}
    public void update(BookCategory bookCategory){
        bookCategoryMapper.updateById(bookCategory);
    }
    //public void update(BookCategory bookCategory){bookCategoryMapper.updateById(bookCategory);}
    public void delete(Integer cid){
        bookCategoryMapper.deleteById(cid);
    }
    public List<BookCategory> findAll(){
        return bookCategoryMapper.selectList(null);
    }
    public BookCategory findDesc(Integer cid){
        return bookCategoryMapper.findBook(cid);
    }






}
