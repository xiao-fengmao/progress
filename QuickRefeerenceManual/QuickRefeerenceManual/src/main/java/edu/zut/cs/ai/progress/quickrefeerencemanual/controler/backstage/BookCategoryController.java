package edu.zut.cs.ai.progress.quickrefeerencemanual.controler.backstage;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Admin.Admin;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Book.Book;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Book.BookCategory;
import edu.zut.cs.ai.progress.quickrefeerencemanual.service.Book.BookService;
import edu.zut.cs.ai.progress.quickrefeerencemanual.service.Book.BookcategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/backstage/bookcategory")
public class BookCategoryController {
    @Autowired
    BookcategoryService bookcategoryService;

    BookService bookService;
    @RequestMapping("/all")
    public ModelAndView all(@RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "10") int size) {
        Page<BookCategory> bookcategoryPage = bookcategoryService.findPage(page, size);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("bookcategoryPage", bookcategoryPage);
        modelAndView.setViewName("/backstage/bookcategory_all");
        return modelAndView;
    }
    @RequestMapping("/add")
    public String add(BookCategory bookCategory) {
        bookcategoryService.add(bookCategory);
        return "redirect:/backstage/bookcategory/all";
    }
    // 查询管理员，跳转到修改页面
    @RequestMapping("/edit")
    public ModelAndView edit(Integer bookcategoryid) {
        BookCategory bookCategory = bookcategoryService.findById(bookcategoryid);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("bookCategory", bookCategory);
        modelAndView.setViewName("/backstage/bookcategory_edit");
        return modelAndView;
    }
    // 修改管理员
    @RequestMapping("/update")
    public String update(BookCategory bookCategory) {
        bookcategoryService.update(bookCategory);
        return "redirect:/backstage/bookcategory/all";
    }
    @RequestMapping("/delete")
    public String delete(Integer bookcategoryid){
        bookcategoryService.delete(bookcategoryid);
        return "redirect:/backstage/bookcategory/all";
    }
    @RequestMapping("/desc")
    public ModelAndView desc(Integer bookcategoryid){
        BookCategory bookCategory = bookcategoryService.findDesc(bookcategoryid);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("bookCategory", bookCategory);
        modelAndView.setViewName("/backstage/bookcategory_desc");
        return modelAndView;
    }



}
