package edu.zut.cs.ai.progress.quickrefeerencemanual.controler.backstage;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Book.BookCategory;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Hardware.HardwareType;
import edu.zut.cs.ai.progress.quickrefeerencemanual.service.Hardware.HardwareTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/backstage/hardwarecategory")
public class HardwareTypeController {
    @Autowired
    HardwareTypeService hardwareTypeService;

    @RequestMapping("/all")
    public ModelAndView all(@RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "10") int size) {
        Page<HardwareType> hardwareTypePage = hardwareTypeService.findPage(page, size);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("hardwareTypePage", hardwareTypePage);
        modelAndView.setViewName("/backstage/hardwarecategory_all");
        return modelAndView;
    }
    @RequestMapping("/add")
    public String add( HardwareType hardwareType) {
        hardwareTypeService.add(hardwareType);
        return "redirect:/backstage/hardwarecategory/all";
    }
    @RequestMapping("/delete")
    public String delete(Integer hardwaretypeid){
        hardwareTypeService.delete(hardwaretypeid);
        return "redirect:/backstage/hardwarecategory/all";
    }
    @RequestMapping("/edit")
    public ModelAndView edit(Integer hardwaretypeid) {
        HardwareType hardwareType = hardwareTypeService.findById(hardwaretypeid);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("hardwareType", hardwareType);
        modelAndView.setViewName("/backstage/hardwarecategory_edit");
        return modelAndView;
    }
    // 修改管理员
    @RequestMapping("/update")
    public String update(HardwareType hardwareType) {
        hardwareTypeService.update(hardwareType);
        return "redirect:/backstage/hardwarecategory/all";
    }
    @RequestMapping("/desc")
    public ModelAndView desc(Integer hardwaretypeid){
        HardwareType hardwareType = hardwareTypeService.findDesc(hardwaretypeid);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("hardwareType", hardwareType);
        modelAndView.setViewName("/backstage/hardwarecategory_desc");
        return modelAndView;
    }


}
