package edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Hardware;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Book.Book;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("HardwareType")
public class HardwareType {
    @TableId
    private Integer hardwaretypeid;
    private String hardwaretyname;
    @TableField(exist = false) // 不是数据库的字段
    private List<Hardware> hardwares; // 集合


//    }
}
