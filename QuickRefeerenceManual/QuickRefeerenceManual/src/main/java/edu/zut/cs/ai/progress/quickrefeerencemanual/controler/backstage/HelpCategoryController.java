package edu.zut.cs.ai.progress.quickrefeerencemanual.controler.backstage;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Help.HelpCategory;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Help.HelpCategory;
import edu.zut.cs.ai.progress.quickrefeerencemanual.service.Help.HelpCategoryService;
import edu.zut.cs.ai.progress.quickrefeerencemanual.service.Help.HelpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/backstage/helpcategory")
public class HelpCategoryController {
    @Autowired
    HelpCategoryService helpCategoryService;
    HelpService helpService;
    
    @RequestMapping("/all")
    public ModelAndView all(@RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "10") int size) {
        Page<HelpCategory> helpcategoryPage = helpCategoryService.findPage(page, size);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("helpcategoryPage", helpcategoryPage);
        modelAndView.setViewName("/backstage/helpcategory_all");
        return modelAndView;
    }
    @RequestMapping("/add")
    public String add(HelpCategory helpCategory) {
        helpCategoryService.add(helpCategory);
        return "redirect:/backstage/helpcategory/all";
    }
    // 查询管理员，跳转到修改页面
    @RequestMapping("/edit")
    public ModelAndView edit(Integer helpcategoryid) {
        HelpCategory helpCategory = helpCategoryService.findById(helpcategoryid);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("helpCategory", helpCategory);
        modelAndView.setViewName("/backstage/helpcategory_edit");
        return modelAndView;
    }
    // 修改管理员
    @RequestMapping("/update")
    public String update(HelpCategory helpCategory) {
        helpCategoryService.update(helpCategory);
        return "redirect:/backstage/helpcategory/all";
    }
    @RequestMapping("/delete")
    public String delete(Integer helpcategoryid){
        helpCategoryService.delete(helpcategoryid);
        return "redirect:/backstage/helpcategory/all";
    }
    @RequestMapping("/desc")
    public ModelAndView desc(Integer helpcategoryid){
        HelpCategory helpCategory = helpCategoryService.findDesc(helpcategoryid);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("helpCategory", helpCategory);
        modelAndView.setViewName("/backstage/helpcategory_desc");
        return modelAndView;
    }



}

