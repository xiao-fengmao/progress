package edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Data;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Data.DataSetName;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DataSetNameMapper extends BaseMapper<DataSetName> {
}
