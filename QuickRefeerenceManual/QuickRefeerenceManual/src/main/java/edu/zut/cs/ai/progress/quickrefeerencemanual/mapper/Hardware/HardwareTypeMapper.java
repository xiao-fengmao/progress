package edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Hardware;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Book.BookCategory;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Hardware.HardwareType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface HardwareTypeMapper extends BaseMapper<HardwareType> {
    HardwareType findhardware(@Param("cid") Integer cid);
}
