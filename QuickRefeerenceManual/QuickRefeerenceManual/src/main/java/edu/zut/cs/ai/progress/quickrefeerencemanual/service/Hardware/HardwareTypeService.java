package edu.zut.cs.ai.progress.quickrefeerencemanual.service.Hardware;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Hardware.HardwareMapper;
import edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Hardware.HardwareTypeMapper;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Book.BookCategory;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Hardware.HardwareType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HardwareTypeService {
    @Autowired
    HardwareTypeMapper hardwareTypeMapper;
    public Page<HardwareType> findPage(int page, int size){
        //页码数，和每页条数，查询条件为null,
        Page selectPage = hardwareTypeMapper.selectPage(new Page(page, size), null);
        return selectPage;
    }
    public void add(HardwareType hardwareType){
        hardwareTypeMapper.insert(hardwareType);
    }
    public void delete(Integer cid){
        hardwareTypeMapper.deleteById(cid);
    }
    public HardwareType findById(Integer bookcategoryid){return hardwareTypeMapper.selectById(bookcategoryid);}
    public void update(HardwareType hardwareType){
        hardwareTypeMapper.updateById(hardwareType);
    }
    public HardwareType findDesc(Integer cid){
        return hardwareTypeMapper.findhardware(cid);
    }
    public List<HardwareType> findAll(){
        return hardwareTypeMapper.selectList(null);
    }

}
