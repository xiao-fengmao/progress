package edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Hardware;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Data.DataSetName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("Hardware")
public class Hardware {
    @TableId
    private Integer id;
    private String hardname;
    private String hardDesc;//详情
    private Integer hardwaretypeid;
    @TableField(exist = false)
    private HardwareType hardwareType;
}
