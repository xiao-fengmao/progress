package edu.zut.cs.ai.progress.quickrefeerencemanual.service.Help;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Help.HelpCategoryMapper;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Help.HelpCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HelpCategoryService {
    @Autowired
    HelpCategoryMapper helpCategoryMapper;
    public Page<HelpCategory> findPage(int page, int size){
        //页码数，和每页条数，查询条件为null,
        Page selectPage = helpCategoryMapper.selectPage(new Page(page, size), null);
        return selectPage;
    }
    public void add(HelpCategory helpCategory){
        helpCategoryMapper.insert(helpCategory);
    }
    public void delete(Integer cid){
        helpCategoryMapper.deleteById(cid);
    }
    public HelpCategory findById(Integer helpcategoryid){return helpCategoryMapper.selectById(helpcategoryid);}
    public void update(HelpCategory helpCategory){
        helpCategoryMapper.updateById(helpCategory);
    }
    public HelpCategory findDesc(Integer cid){
        return helpCategoryMapper.findhelp(cid);
    }
    public List<HelpCategory> findAll(){
        return helpCategoryMapper.selectList(null);
    }

}
