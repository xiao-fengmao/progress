package edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Admin;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Admin.Permission;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface PermissionMapper extends BaseMapper<Permission> {
    // 查询角色拥有的所有权限的id
    List<Integer> findPermissionIdByRole(Integer rid);
}

