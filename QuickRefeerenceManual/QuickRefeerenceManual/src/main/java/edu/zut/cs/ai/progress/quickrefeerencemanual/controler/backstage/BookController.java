package edu.zut.cs.ai.progress.quickrefeerencemanual.controler.backstage;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Book.Book;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Book.BookCategory;
import edu.zut.cs.ai.progress.quickrefeerencemanual.service.Book.BookService;
import edu.zut.cs.ai.progress.quickrefeerencemanual.service.Book.BookcategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
@Controller
@RequestMapping("/backstage/book")
public class BookController {
    @Autowired
    private BookService bookService;
    @Autowired
    private BookcategoryService bookcategoryService;


    @RequestMapping("/all")
    public ModelAndView all(@RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "5") int size) {
        Page<Book> bookPage = bookService.findPage(page, size);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("bookPage", bookPage);
        modelAndView.setViewName("/backstage/book_all");
        return modelAndView;
    }
    @RequestMapping("/addPage")
    public ModelAndView addPage() {
        // 查询所有产品类别
        List<BookCategory> categoryList = bookcategoryService.findAll();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("categoryList", categoryList);
        modelAndView.setViewName("/backstage/book_add");
        return modelAndView;
    }

    @RequestMapping("/add")
    public String add(Book book) {
        bookService.add(book);
        return "redirect:/backstage/book/all";
    }
    @RequestMapping("/delete")
    public String delete(Integer bookid){
        bookService.delete(bookid);
        return "redirect:/backstage/book/all";
    }

}
