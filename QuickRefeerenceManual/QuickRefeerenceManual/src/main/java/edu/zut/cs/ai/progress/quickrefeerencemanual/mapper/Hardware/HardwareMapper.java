package edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Hardware;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Book.Book;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Hardware.Hardware;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface HardwareMapper extends BaseMapper<Hardware> {
    Page<Hardware> findhardwarePage(Page<Hardware> page);
}


