package edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Book;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Admin.Admin;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Book.Book;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Book.BookCategory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BookCategoryMapper extends BaseMapper<BookCategory> {
    BookCategory findBook(@Param("cid") Integer cid);
    //@Param("cid") 注解的作用是将方法参数 Integer cid 与 SQL 语句中的 #{cid} 进行关联。
    // 这样，在执行 SQL 语句时，MyBatis 将会使用传入的 cid 值替换 #{cid}，
    // 从而实现参数的传递和查询条件的匹配
}

//    一对多的多表查
