DROP DATABASE IF EXISTS progress;
create DATABASE progress;
use progress;
-- Admin---------------------------------------------------------------------------------
CREATE TABLE Admin (
                       aid INT PRIMARY KEY AUTO_INCREMENT,
                       username VARCHAR(255),
                       password VARCHAR(255),
                       phoneNum VARCHAR(20),
                       email VARCHAR(255),
                       status TINYINT(1),
                       INDEX idx_status (status)
);
-- 向Admin表插入数据
INSERT INTO Admin (username, password, phoneNum, email, status)
VALUES ('admin1', 'password1', '1234567890', 'admin1@example.com', 1),
       ('admin2', 'password2', '0987654321', 'admin2@example.com', 0);

DROP TABLE IF EXISTS `admin_role`;
CREATE TABLE `admin_role` (
                              `aid` varchar(32) NOT NULL,
                              `rid` varchar(32) NOT NULL,
                              PRIMARY KEY (`aid`,`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `admin_role`(`aid`, `rid`) VALUES ('1', '1'), ('2', '2');


CREATE TABLE `permission` (
                              `pid` int(32) NOT NULL AUTO_INCREMENT,
                              `permissionName` varchar(50) DEFAULT NULL,
                              `permissionDesc` varchar(50) DEFAULT NULL,
                              PRIMARY KEY (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `permission` */

insert  into `permission`(`pid`,`permissionName`,`permissionDesc`) values (1,'查询管理员','/admin/all'),(2,'查询角色','/role/all'),(3,'查询权限','/permission/all'),(4,'新增权限','/permission/add');






-- book--
DROP TABLE IF EXISTS bookcategory;
CREATE TABLE bookcategory (
                              bookcategoryid INT PRIMARY KEY AUTO_INCREMENT,
                              bookcategoryname VARCHAR(255)
);
DROP TABLE IF EXISTS book;

CREATE TABLE `book` (
                        `bookid` INT(11) PRIMARY KEY AUTO_INCREMENT,
                        `bookname` VARCHAR(255) DEFAULT NULL,
                        `bookimage` VARCHAR(255) DEFAULT NULL,
                        `bookDesc` VARCHAR(255) DEFAULT NULL,
                        `Tel` VARCHAR(255) DEFAULT NULL,
                        `price` DECIMAL(10,2) DEFAULT NULL,
                        `bookcategoryid` INT(11) DEFAULT NULL
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4;
INSERT INTO bookcategory (bookcategoryname) VALUES ('小说');
INSERT INTO bookcategory (bookcategoryname) VALUES ('科幻');
INSERT INTO bookcategory (bookcategoryname) VALUES ('历史');
INSERT INTO book ( bookname, bookimage, bookDesc, Tel, price, bookcategoryid)
VALUES ( '三体', '/upload/img.png', '《三体》是刘慈欣的长篇科幻小说，讲述了地球人类文明与三体文明之间的斗争。\n', '123456789', 39.99, 2);
INSERT INTO book ( bookname, bookimage, bookDesc, Tel, price, bookcategoryid)
VALUES ( '平凡的世界', '/upload/img_1.png', '《平凡的世界》是路遥的代表作之一，讲述了中国社会主义改革开放前夕的山村故事。\n', '987654321', 49.99, 1);

INSERT INTO book (bookname, bookimage, bookDesc, Tel, price, bookcategoryid)
VALUES ('史记', '/upload/img_2.jpg', '《史记》是中国第一部纪传体通史，被誉为''二十四史''之首.', '111122223333', 29.99, 3);
-- hardware---------------------
DROP TABLE IF EXISTS Hardware;
CREATE TABLE Hardware (
                          id INT PRIMARY KEY AUTO_INCREMENT,
                          hardname VARCHAR(255),
                          hardDesc TEXT DEFAULT NULL,
                          hardwaretypeid INT
);
INSERT INTO Hardware (hardname, hardDesc, hardwaretypeid)
VALUES ('AMD Radeon RX 6800 XT', '该显卡由 AMD 推出，是一款面向 PC 游戏玩家的高性能图形处理器。它拥有 4,608 个流处理器、16 GB GDDR6 显存以及高达 512 GB/s 的内存带宽。', 1),
       ('Nvidia GeForce RTX 3080', '该显卡由 Nvidia 推出，是目前市场上最强大的图形处理器之一。它拥有 8,704 个 CUDA 核心，10 GB GDDR6X 显存，以及高达 760 GB/s 的内存带宽。', 1),
       ('Nvidia GeForce GTX 1660 Ti', '该显卡是一款中端图形处理器，适合一般游戏和日常应用。它拥有 1,536 个 CUDA 核心、6 GB GDDR6 显存以及高达 288 GB/s 的内存带宽。', 1),
         ('Corsair Vengeance LPX', '这是一款受欢迎的高性能内存，适用于游戏和计算机应用。它具有高频率和低延迟，并且可提供稳定的性能。常见的容量包括 8GB、16GB、32GB 和 64GB。', 4),
       ('G.Skill Trident Z RGB', '这是一款外观酷炫的内存，具有高频率和低延迟。它不仅在性能上表现出色，还提供了可自定义的RGB照明效果，增添了计算机的个性化。常见的容量包括 8GB、16GB、32GB 和 64GB。。', 4),
       ('Nvidia GeForce RTX 3080', '这是一款高性能的显卡，拥有大量的CUDA核心、高显存容量和带宽，适用于游戏和专业图形应用。它支持光线追踪技术，并具有出色的性能表现。', 3),
       ('Intel Core i9-11900K', '这是一款高性能的桌面处理器，适用于游戏和专业应用。它采用了Intel的最新技术，具有高核心数量、高频率和低延迟，提供卓越的计算性能', 2),
       ('AMD Ryzen 9 5950X', '这是一款面向高性能桌面计算机的处理器，适用于游戏、多任务处理和专业工作。它采用AMD Zen 3架构，具有大量的核心和线程，提供出色的多线程性能。', 2),
       ('Intel Core i7-11700K', '这是一款中高端的桌面处理器，适用于游戏和一般应用。它具有较高的核心数量和频率，提供良好的性能和功耗比。', 2);

DROP TABLE IF EXISTS HardwareType;
CREATE TABLE HardwareType (
                              hardwaretypeid INT PRIMARY KEY AUTO_INCREMENT,
                              hardwaretyname VARCHAR(255)
);
INSERT INTO HardwareType (hardwaretyname) VALUES ('显卡');
INSERT INTO HardwareType (hardwaretyname) VALUES ('CPU');
INSERT INTO HardwareType (hardwaretyname) VALUES ('GPU');
INSERT INTO HardwareType (hardwaretyname) VALUES ('内存');

-- help---------------
DROP TABLE IF EXISTS Help;
CREATE TABLE Help (
                      helpid INT PRIMARY KEY AUTO_INCREMENT,
                      time VARCHAR(255),
                      helpdesc TEXT,
                      Tel VARCHAR(255),
                      price DECIMAL(10, 2),
                      statue BOOLEAN,
                      helpcategoryid INT
);
DROP TABLE IF EXISTS HelpCategory;
CREATE TABLE HelpCategory (
                              helpcategoryid INT PRIMARY KEY AUTO_INCREMENT,
                              helpcategoryname VARCHAR(255)
);
