package edu.zut.cs.ai.progress.quickrefeerencemanual.mapper;

import edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Book.BookCategoryMapper;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Book.BookCategory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class BookCategoryMapperTest {
    @Autowired
    BookCategoryMapper bookCategoryMapper;

    @Test
    public void findbook() {
        BookCategory bookCategory = bookCategoryMapper.findBook(1);
        if(bookCategory != null) {
            System.out.println(bookCategory); // 打印查询结果
        } else {
            System.out.println("未找到对应的书籍类别");
        }
        // 这里也可以添加断言来验证查询结果
    }
}
