package edu.zut.cs.ai.progress.quickrefeerencemanual.mapper;

import edu.zut.cs.ai.progress.quickrefeerencemanual.mapper.Admin.AdminMapper;
import edu.zut.cs.ai.progress.quickrefeerencemanual.pojo.Admin.Admin;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class AdminMapperTest {
    @Autowired
   public AdminMapper adminMapper;
    @Test
    public void AdminAdd(){
        Admin admin=new Admin("王建国","123456","16689001234","123@qq.com",true);
        adminMapper.insert(admin);
    }
}
