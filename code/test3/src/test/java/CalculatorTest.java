public class CalculatorTest {

    public static void main(String[] args) {
        testAdd();
        testSubtract();
    }

    public static void testAdd() {
        Calculator calculator = new Calculator();
        int result = calculator.add(2, 3);
        System.out.println(result);
        assert result == 5 : "Addition test failed";
    }

    public static void testSubtract() {
        Calculator calculator = new Calculator();
        int result = calculator.subtract(5, 3);
        assert result == 2 : "Subtraction test failed";
    }
}
