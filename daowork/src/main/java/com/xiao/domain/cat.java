package com.xiao.domain;

public class cat {
    private int id;
    private String name;
    private String sex;
    private int age;
    //private String category;
    private String health;
    //属性

    public cat() {

    }
    //构造方法
    public cat(String name, String sex, int age,String health) {
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.health = health;
    }
    //构造方法
    public cat(int id, String name, String sex, int age, String health) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.health = health;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }



    public String getHealth() {
        return health;
    }

    public void setHealth(String health) {
        this.health = health;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", age=" + age +
                ", health='" + health + '\'' +
                '}';
    }
}
